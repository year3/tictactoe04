package com.ntc.lab04;

/**
 *
 * @author L4ZY
 */
public class OXOOP {

    public static void main(String[] args) {
        Game game = new Game();
        game.showWelcome();
        game.newBoard();
        while (true) {
            game.showTable();
            game.showTurn();
            game.inputRowCol();
            if (game.isFinish()) {
                game.showTable();
                game.showResult();
                game.showStat();
                System.out.println("=====================");
                if (game.next() == true) {
                    game.newBoard();
                } else {
                    break;
                }
            }

        }
    }

}
